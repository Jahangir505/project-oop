
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/project/config.php");
use Jahangir\Database;

$id = $_GET['id'];
$database = new Database();

$data = $database->getById($id, 'sliders');
?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/project/admin/partials/header.php'); ?>
<!-- /.navbar -->

<!-- Main Sidebar Container -->

<?php adminpartial('sidebar.php'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Slider Update</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Slider Update</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <form action="store.php" method="POST">
        <div class="row">
           
            <div class="col-md-8 offset-md-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Slider Update</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputName">Title</label>
                                <input type="text" name="title" value="<?= $data['title'] ?>" id="inputName" class="form-control">
                            </div>
                        
                            
                            <div class="form-group">
                                <label for="inputClientCompany">Cation</label>
                                <input type="text" name="caption" value="<?= $data['caption'] ?>" id="inputClientCompany" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="inputProjectLeader">Caption Title</label>
                                <input type="text" name="caption_title" value="<?= $data['caption_title'] ?>" id="inputProjectLeader" class="form-control">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <input type="hidden" value="<?= $data['id'] ?>" name="id" />
                            <input type="submit" value="Update" class="btn btn-success float-right">
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
           
           
        </div>
        </form>
       
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php adminpartial("footer.php"); ?>