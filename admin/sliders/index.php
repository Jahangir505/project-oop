<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/project/admin/partials/header.php'); ?>
<!-- /.navbar -->

<?php
// include_once($_SERVER['DOCUMENT_ROOT'] . "/project/config.php");

use Jahangir\Database;
use Jahangir\Utility\Debugger;

$database = new Database();

$sliders = $database->showData("sliders");

// Debugger::dd($sliders);

?>

<!-- Main Sidebar Container -->

<?php adminpartial('sidebar.php'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Slider Management</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Sliders</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Sliders List</h3>

        <div class="card-tools">
          <a type="a" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </a>
          <a type="a" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </a>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Title</th>
              <th>Caption</th>
              <th>Caption Title</th>
              <th style="width: 200px">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($sliders as $key => $slide) : ?>
              <tr key="<?= $key ?>">
                <td><?= $key + 1; ?></td>
                <td><?= $slide['title']; ?></td>
                <!-- <td>
                  <img src="../<?= $slide['image']; ?>" alt="<?= $slide['title']; ?>" height="50" />
                </td> -->
                <td><?= $slide['caption']; ?></td>
                <td><?= $slide['caption_title']; ?></td>
                <td>
                  <a href="edit.php?id=<?= $slide['id']; ?>" class="btn bg-primary btn-sm m-1">Edit</a>
                  <a href="show.php?id=<?php $slide['id']; ?>" class="btn bg-info btn-sm m-1">View</a>
                  <a class="btn bg-danger btn-sm m-1">Delete</a>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <ul class="pagination pagination-sm m-0 float-right">
          <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
        </ul>
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php adminpartial('footer.php'); ?>