<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/project/config.php");
use Jahangir\Database;

$id = $_GET['id'];
?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/project/admin/partials/header.php'); ?>
<!-- /.navbar -->

<!-- Main Sidebar Container -->

<?php adminpartial('sidebar.php'); ?>
<!-- Content Wrapper. Contains page content -->

<?php
    $database = new Database();

    $single_data = $database->getById($id, 'sliders');

    // dd($single_data);

?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Slider Management</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Sliders</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Sliders List</h3>

        <div class="card-tools">
          <a type="a" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </a>
          <a type="a" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </a>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Title</th>
              <th>Caption</th>
              <th>Caption Title</th>
              <th style="width: 200px">Action</th>
            </tr>
          </thead>
          <tbody>
           <tr>
               <td></td>
               <td><?= $single_data['title']; ?></td>
               <td><?= $single_data['caption']; ?></td>
               <td><?= $single_data['caption_title']; ?></td>
           </tr>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <ul class="pagination pagination-sm m-0 float-right">
          <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
        </ul>
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php adminpartial("footer.php"); ?>