<li class="nav-item">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-copy"></i>
    <p>
      Slider Management
      <i class="fas fa-angle-left right"></i>
      <span class="badge badge-info right">2</span>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="sliders/index.php" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>All Sliders</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="sliders/create.php" class="nav-link">
        <i class="far fa-circle nav-icon"></i>
        <p>Create Slider</p>
      </a>
    </li>

  </ul>
</li>