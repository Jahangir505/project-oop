<?php

ini_set('display_errors', 'on');
$foldername = 'project';

$docroot = $_SERVER['DOCUMENT_ROOT']  . "/" . $foldername;
$docroot_admin = $_SERVER['DOCUMENT_ROOT'] . "/"  . $foldername . "/admin";
$docroot_frontend = $_SERVER['DOCUMENT_ROOT'] . "/"  . $foldername . "/frontend";


$webroot = "http://php-oop.test/" . $foldername;
$webroot_admin = $webroot . "/admin";
$webroot_frontend = $webroot . "/frontend";

$url_home = $webroot . "/frontend/home.php";
$url_admin = $webroot . "/admin/login.php";
$url_admin_dashboard = $webroot . "/admin/dashboard.php";

$upload_uri = $docroot . "/storage/uploads";

$autoload = $docroot . "/vendor/autoload.php";
include_once($autoload);

use Jahangir\Utility\Debugger;
use Jahangir\Utility\Helper;

function dd($value)
{
    Debugger::dd($value);
}

function r($url)
{
    Helper::redirect($url);
}

function adminpartial($filename)
{
    global $docroot_admin; // we shall not use this. later we shall refactor config into class
    Helper::loadPartial($filename, $docroot_admin);
}

function frontpartial($filename)
{
    global $docroot_frontend;
    Helper::loadPartial($filename, $docroot_frontend);
}
