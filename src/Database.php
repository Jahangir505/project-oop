<?php

namespace Jahangir;

use Jahangir\Utility\Helper;

class Database
{
    private $host = "localhost";
    private $user = "root";
    private $db = "php_oop";
    private $pass = "";
    private $conn;

    public function __construct()
    {
        $this->conn = new \PDO("mysql:host=" . $this->host . ";dbname=" . $this->db, $this->user, $this->pass);
    }

    public function showData($table)
    {
        $sql = "SELECT * FROM $table";
        $q = $this->conn->query($sql) or die("failed!");

        while ($r = $q->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $r;
        }
        return $data;
    }

    public function getById($id, $table)
    {
        $sql = "SELECT * FROM $table WHERE id = :id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id' => $id));
        $data = $q->fetch(\PDO::FETCH_ASSOC);
        return $data;
    }



    public function storeSlider($data)
    {
        $sql = "INSERT INTO `sliders` (`id`, `title`, `image`, `caption_title`, `caption`, `created_at`, `updated_at`) VALUES (NULL, :title, :picture, :caption_title, :caption, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";

        // d($sql);
        // d($data);
        $stmt = $this->conn->prepare($sql);
        $result = $stmt->execute($data);

        if ($result) {
            echo "Data Submit Successfully!";
            Helper::redirect('http://php-oop.test/project/admin/sliders/index.php');
        } else {
            echo "Data Submit Faild!";
        }
    }
}
