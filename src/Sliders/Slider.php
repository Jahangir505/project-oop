<?php
namespace Jahangir\Sliders;

use Jahangir\Utility\Debugger;
use Jahangir\Utility\Helper;

class Slider
{
    public $title = null;
    public $picture = null;
    public $captiontitle = null;
    public $caption = null;

    public $slidesLimit = 2;

    public function __construct($totalSlides = 2)
    {
        // initializing variables
        // connecting to database
        // file open etc..
        if ($totalSlides > 0) {
            $this->slidesLimit = $totalSlides;
        }
    }

   
    public function get()
    {
        $splittedSlides = array_chunk($this->all(), $this->slidesLimit);
        return $splittedSlides[0];
    }

    public function __destruct()
    {
        echo "I am dying.....";
    }

    public function __get($property)
    {
        echo "Hello... you are trying to access $property that does not exists";
    }

    public function __set($prop, $value)
    {
        echo $prop, $value;
    }
}
