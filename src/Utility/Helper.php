<?php

namespace Jahangir\Utility;

class Helper extends Utility
{

    public static function redirect($url)
    {
        header('location:' . $url);
    }

    public static function loadPartial($partialname, $path)
    {
        global $webroot_admin;
        include_once($path . "/partials/" . $partialname);
    }
}
